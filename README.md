# Settlers of Catan - Compiled files for laser cutting and printing

Included are the laser files for cutting the original base game for 4 players. There are separate files for cutting the player pieces one at a time if desired. If using the optional harbormaster card, add one max victory point to win the game. The robber has 2 different bases you can glue them to for 1/8" or 1/4" material.

The dimensions of the drawings are roughly as follows
* Game board pieces - 13" x 15.5" or 328mm x 394mm
* Player pieces - Single player bunched - 5.7" x 1.52" or 144mm x 39mm
* Player pieces - Single player long - 12.8" x 0.8" or 300mm x 20mm
* Player pieces - All together - 12.8" x 3.27" or 300mm x 83mm

Printing onto a cardstock with a rustic color works well and doesn't require double sided printing.

![Sample][logo]

[logo]: files/example.jpg "Sample"

The sheep text is easily edited with a vector graphics software like the free Inkscape. The original art was hard to read, so if you feel like providing better art, feel free to fork or submit a pull request and I'll change it.

## Sources

This is compiled from various sources online including
* [https://github.com/BryantCabrera/Settlers-of-Catan](https://github.com/BryantCabrera/Settlers-of-Catan)
* [https://glowforge.com/pdfs/game-board.pdf](https://glowforge.com/pdfs/game-board.pdf) - note this has a number of innaccuracies to the original game like a missing 6, extra ore and brick, and 2:1 any ports. Reference [here](https://catan.fandom.com/wiki/Catan)